# GFN-Framework

This is a small PHP-Framework by the [German-Furs Network](https://www.german-furs.net/), to build a custom website.

## Requirements

- [PHP 8.3+](https://www.php.net/)

## Features

- [x] Template Engine
  - write own templates in plain **.phtml** files
- [x] Cache System
- [x] Lightweight Loader
- [x] Exceptions
- [ ] Database Connector
  - [x] MySQL / MariaDB 
  - [x] MSSQL 
  - [x] PostgreSQL 
  - [x] SQLite 
  - [ ] Redis 
- [x] Active Record in fully OOP-Style
- [ ] E-Mail sending

## Repository

Look into [GitLab](https://gitlab.german-furs.net/german-furs/gfn-framework) for future updates!

## Getting started
### Installing via Composer
**Important**: Set the _minimum-stability_ of your project to **dev**, because this Framework is still in Alpha!
#### CLI
```shell
composer require german-furs/gfn-framework:"dev-main@dev"
```
#### Add manuel to composer.json
```
"require": {
    "german-furs/gfn-framework": "dev-main"
}
```
Now you need to update your composer.
```shell
composer update
```
### Constants defined by User
```php
// Switch environment to development
const DEV_ENV = true;
// Should logs save as file to PATH_TMP
const LOG_TO_FILE = true;
// Should logs save into the database
const LOG_TO_DB = false;
// Set the used timezone
const CORE_TIME_ZONE = 'UTC';
// Should the Cache been used
const TEMPLATE_CACHE_ENABLED = true;
// E-Mail-Address for mails after exceptions
const EXCEPTION_MAIL_TO = '';
// E-Mail-Address of the sender
const EXCEPTION_MAIL_FROM = '';
// E-Mail-Address for mails after defined events
const SYSTEM_MAIL_FROM = '';
// Host used for the database connection
const APP_DB_HOST = '';
// Port used for the database connection
const APP_DB_PORT = '';
// Databasename used for the database connection
const APP_DB_NAME = '';
// User used for the database connection
const APP_DB_USER = '';
// Password used for the database connection
const APP_DB_PASS = '';
// Absolute path to the App directory
const PATH_APP = 'path/to/App' . DIRECTORY_SEPARATOR;
// Absolute path to the directory for the modules
const PATH_MODULES = PATH_APP . 'Modules' . DIRECTORY_SEPARATOR;
// Absolute path to the directory for the templates
const PATH_TEMPLATES = PATH_APP . 'Templates' . DIRECTORY_SEPARATOR;
// Absolute path to the directory for the log files
const PATH_LOG = PATH_APP . 'log' . DIRECTORY_SEPARATOR;
// Absolute path to the directory for the temporary data
const PATH_TMP = 'path/to/tmp' . DIRECTORY_SEPARATOR;
// Absolute path to the directory for the cache files
const PATH_CACHE = PATH_TMP . 'cache' . DIRECTORY_SEPARATOR;
// Absolute path to the sqlite-database
const APP_SQLITE_FILE_PATH = 'path/to/sqlite.db';
```

### Initialise the Framework
To use the Framework, you have to the constants and create the directories in your project. Copy the Templates from the _Templates-Directory_ of this Framework or create your own.
```php
use GfnFramework\Core\Load;
use GfnFramework\Core\Template\Engine;

// Load the Autoload of Composer
require_once 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php'

// Initialise of the Framework
Load::init();

// Example for a basic output of a page
$header_data = [
	'<meta charset="utf-8">',
	'<meta name="viewport" content="width=device-width, initial-scale=1.0">',
	'<title>Simple Example Page by GfnFramework</title>',
	'<style>body{background-color: #ccc}</style>'
];
$body_data = [
	'<h1>Headline</h1>',
	'<h2>Subline</h2>',
	'<p>Basic text of dreams.</p>',
];
$tpl = new Engine();
$tpl->addHeaderParameters($header_data);
$tpl->addBodyParameters($body_data);
$tpl->printPage();
```

**TBC**