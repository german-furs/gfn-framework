<?php declare(strict_types=1);

/**
 * Returns the last calling class.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 *
 * @return	string
 */
function getCallingClass() : string {

	$trace = debug_backtrace();

	return $trace[1]['class'];
}