<?php declare(strict_types=1);

/**
 * A better debug output.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 *
 * @see		var_dump()
 *
 * @param	mixed	...$values	the parameters to show
 *
 * @return	void
 *
 */
function varDump(mixed ...$values) : void {

	$bt		= debug_backtrace();
	$file	= $bt[0]['file'];
	$line	= $bt[0]['line'];

	// change XDebug parameters to show more information
	ini_set('xdebug.var_display_max_depth', '10');
	ini_set('xdebug.var_display_max_children', '256');
	ini_set('xdebug.var_display_max_data', '1024');

	echo '<div style=\'z-index:10001;position:relative;background-color:white\'>';
	echo '<fieldset style=\'border:1px solid red;\'>';
	echo '<legend style=\'font-size:11px\'>Called at ' . $file . ':' . $line . '</legend>';

	foreach ($values as $value) {
		var_dump($value);
	}

	echo '</fieldset>';
	echo '</div>';
}

/**
 * Debug output to a file.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 *
 * @param	mixed	...$values	the parameters to show
 *
 * @return	void
 */
function varExport(mixed ...$values) : void {

	$bt		= debug_backtrace();
	$file	= $bt[0]['file'];
	$line	= $bt[0]['line'];
	$path	= PATH_LOG . 'debug.log';

	foreach ($values as $value) {
		error_log('[' . date('d-m-Y H:i:s') . '][' . $file . ':' . $line . ']', 3, $path);
		error_log(PHP_EOL, 3, $path);
		error_log(var_export($value, true), 3, $path);
		error_log(PHP_EOL, 3, $path);
	}
}