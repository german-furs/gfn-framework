<?php declare(strict_types=1);

namespace GfnFramework\Core;

use GfnFramework\Core\Exceptions\GfnMailException;
use Exception;
use PHPMailer\PHPMailer;

/**
 * Base E-Mail class.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Email extends PHPMailer\PHPMailer {

	/**
	 * Sends a mail.
	 *
	 * @return	bool
	 *
	 * @throws	GfnMailException
	 */
	public function send() : bool {

		try {
			return parent::send();
		} catch (Exception | PHPMailer\Exception $e) {
			throw new GfnMailException($e->getMessage(), $e->getCode(), $e);
		}
	}
}