<?php declare(strict_types=1);

namespace GfnFramework\Core\Abstracts;

use GfnFramework\Core\Exceptions\GfnException;
use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;
use ReflectionClass;
use ReflectionException;

/**
 * Abstract class to restrict the number of instances that can be created from a resource consuming class to only one.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
abstract class Singleton {

	/**
	 * Holds all object instances.
	 *
	 * @var	self[]
	 */
	protected static array $_instances = [];

	/**
	 * Holds the reflection of the called class.
	 *
	 * @var	ReflectionClass
	 */
	private static ReflectionClass $_reflector;

	/**
	 * Create und save the instances.
	 *
	 * @param	mixed	...$constructor_arguments	all arguments for the constructor
	 *
	 * @return	static
	 *
	 * @throws	GfnException
	 * @throws	GfnInvalidArgumentException
	 */
	public static function getInstance(...$constructor_arguments) : static {

		try {
			self::$_reflector	= new ReflectionClass(get_called_class());
			$constructor		= self::$_reflector->getConstructor();
			$params				= [];

			// validate the given parameters
			if (count($constructor_arguments) > 0 && $constructor->getNumberOfParameters() > 0) {
				foreach($constructor->getParameters() AS $key => $arg) {
					if($constructor_arguments[$key]) {
						$params[$arg->name] = $constructor_arguments[$key];
					} else {
						$params[$arg->name] = null;
					}
				}
			}

			// name of the called class
			$class_name = get_called_class();

			return self::makeInstance($class_name, $params);
		} catch (ReflectionException $e) {
			throw new GfnException($e->getMessage());
		}
	}

	/**
	 * Remove all instances.
	 *
	 * @return	void
	 */
	public static function clearInstances() : void {

		self::$_instances = [];
	}

	/**
	 * Create the cache of the instance and return it.
	 *
	 * @param	string	$class_name	Name of the class
	 * @param	array	$params		Parameters
	 *
	 * @return	static
	 *
	 * @throws	GfnInvalidArgumentException
	 * @throws	ReflectionException
	 */
	private static function makeInstance(string $class_name, array $params = []) : static {

		if (self::_existsInstance($class_name, $params)) {
			return self::$_instances[$class_name][self::_getParamsHash($params)];
		}

		// Never instantiate with a beginning backslash, otherwise things like singletons won't work.
		if ($class_name[0] === '\\') {
			throw new GfnInvalidArgumentException('$class_name "' . $class_name . '" darf nicht mit einem Backslash beginnen.');
		}

		return self::$_instances[$class_name][self::_getParamsHash($params)] = self::$_reflector->newInstanceArgs($params);
	}

	/**
	 * Check if is an instance already known.
	 *
	 * @param	string	$class_name	Name of the class
	 * @param	array	$params		Parameters
	 *
	 * @return	bool
	 */
	private static function _existsInstance(string $class_name, array $params = []) : bool {

		return isset(self::$_instances[$class_name][self::_getParamsHash($params)]);
	}

	/**
	 * Create a hash from the parameters.
	 *
	 * @param	array	$params	Parameters
	 *
	 * @return	string
	 */
	private static function _getParamsHash(array $params) : string {

		return hash('xxh3', serialize($params));
	}
}