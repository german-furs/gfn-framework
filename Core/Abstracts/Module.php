<?php declare(strict_types=1);

namespace GfnFramework\Core\Abstracts;

use GfnFramework\Core\Exceptions\GfnModuleException;
use ReflectionClass;

/**
 * Base class to build a module.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
abstract class Module extends Singleton {

	const int GET_REQUEST_ARRAY		= 0;
	const int POST_REQUEST_ARRAY	= 1;

	/**
	 * @var	string	Holds the internal name of the module
	 */
	protected string $_name_intern;

	/**
	 * @var	string	Holds the full name of the module
	 */
	protected string $_name_extern;

	/**
	 * @var	array	Holds the config of the module
	 */
	protected array $_config;

	/**
	 * @var	array	Holds the names of all modules
	 */
	protected static array $_modules;

	/**
	 * @var	array	Holds the name of all valid keys of the $_GET
	 */
	protected array $_valid_get_keys = [];

	/**
	 * @var	array	Holds the name of all valid keys of the $_POST
	 */
	protected array $_valid_post_keys = [];

	/**
	 * Holds all valid GET-Requests.
	 *
	 * @var	array
	 */
	protected array $_get_requests = [];

	/**
	 * Holds all valid POST-Requests.
	 *
	 * @var	array
	 */
	protected array $_post_requests = [];

	/**
	 * Holds the names of needed keys inside the config
	 *
	 * @var	string[]
	 */
	private array $_config_needed_keys = [
		'module_name_extern',
		'valid_get_keys',
		'valid_post_keys'
	];

	/**
	 * Returns the list of all modules.
	 *
	 * @return	array
	 */
	public static function getAllModules() : array {

		if (!empty(self::$_modules)) {
			return self::$_modules;
		}

		return self::$_modules = array_map('basename', glob(PATH_MODULES . '*' , GLOB_ONLYDIR));
	}

	/**
	 * Base-Constructor of all module-classes.
	 *
	 * @throws	GfnModuleException
	 */
	public function __construct() {

		$called_class_reflection	= new ReflectionClass(get_called_class());
		$this->_name_intern			= $called_class_reflection->getShortName();

		if (!is_dir(PATH_MODULES . $this->_name_intern)) {
			throw new GfnModuleException('Unknown module');
		}

		$this->_loadConfig();
	}

	/**
	 * Returns the config of the module.
	 *
	 * @return	array
	 */
	public function getConfig() : array {

		return $this->_config;
	}

	/**
	 * Returns all save User-Input.
	 *
	 * @param	int		$from	GET or POST
	 *
	 * @return	array
	 */
	public function getRequest(int $from = self::GET_REQUEST_ARRAY) : array {

		if ($from === self::GET_REQUEST_ARRAY && !empty($this->_get_requests)) {
			return $this->_get_requests;
		} elseif ($from === self::POST_REQUEST_ARRAY && !empty($this->_post_requests)) {
			return $this->_post_requests;
		}

		$request_output	= [];
		$request_result	= [];

		if ($from === self::GET_REQUEST_ARRAY) {
			$request_output = $_GET;
		} elseif ($from === self::POST_REQUEST_ARRAY) {
			$request_output = $_POST;
		}

		if (empty($request_output)) {
			return $request_result;
		}

		foreach ($this->_valid_get_keys as $valid_key) {
			if (array_key_exists($valid_key, $request_output)) {
				$request_result[$valid_key] = $this->_getSaveUserContent($valid_key, $request_output);
			}
		}

		if ($from === self::GET_REQUEST_ARRAY) {
			$this->_get_requests = $request_result;
		} elseif ($from === self::POST_REQUEST_ARRAY) {
			$this->_post_requests = $request_result;
		}

		return $request_result;
	}

	/**
	 * Loads the config of the module and set variables of the module.
	 *
	 * @return	void
	 *
	 * @throws	GfnModuleException
	 */
	private function _loadConfig() : void {

		$config_path = PATH_MODULES . $this->_name_intern . DIRECTORY_SEPARATOR . 'config.php';

		if (!is_file($config_path)) {
			throw new GfnModuleException('Module-Config missing');
		}

		$_CONF = [];
		include_once $config_path;

		foreach ($this->_config_needed_keys as $key) {
			if (!array_key_exists($key, $_CONF)) {
				throw new GfnModuleException('Config-Value for ' . $key . ' missing');
			}
		}

		$this->_config		= $_CONF;
		$this->_name_extern	= $this->_config['module_name_extern'];

		if (array_key_exists('valid_get_keys', $this->_config)) {
			$this->_valid_get_keys = $this->_config['valid_get_keys'];
		}

		if (array_key_exists('valid_post_keys', $this->_config)) {
			$this->_valid_post_keys = $this->_config['valid_post_keys'];
		}
	}

	/**
	 * Validate bad User-Input.
	 * TODO: Needs improvements
	 *
	 * @param	string	$key			the current key
	 * @param	array	$request_output	the GET or POST array
	 *
	 * @return	string
	 */
	private function _getSaveUserContent(string $key, array $request_output) : string {

		return htmlspecialchars(stripslashes(trim($request_output[$key])));
	}
}