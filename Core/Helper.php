<?php declare(strict_types=1);

namespace GfnFramework\Core;

/**
 * Class to hold methods with usefully tasks.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Helper {

	/**
	 * Returns the readable size.
	 * Supports base 2 (IEC 60027-2) and base 10 (IEC 80000-13).
	 *
	 * @param	int	$bytes	the size
	 * @param	int	$base	base of 2 or 10
	 *
	 * @return	string
	 */
	public static function getReadableByteSize(int $bytes, int $base = 2) : string {

		$num		= ($base === 2 ? 1024 : 1000);
		$exponent	= floor(log($bytes, $num));
		$size		= strval(round($bytes / pow($num, $exponent), 3));
		$extension	= (
			$base === 2 ?
				[
					'B',
					'KiB',
					'MiB',
					'GiB',
					'TiB',
					'PiB',
					'EiB',
					'ZiB',
					'YiB'
				]
			:
				[
					'B',
					'KB',
					'MB',
					'GB',
					'TB',
					'PB',
					'EB',
					'ZB',
					'YB'
				]
		);

		return $size . ' ' . $extension[$exponent];
	}

	/**
	 * Returns the used Memory.
	 *
	 * @param	int	$base	base of 2 or 10
	 *
	 * @see		self::getReadableByteSize()
	 *
	 * @return	string
	 */
	public static function getReadablePeakUsage(int $base = 2) : string {

		return self::getReadableByteSize(memory_get_peak_usage(), $base);
	}

	/**
	 * Returns the processed time since start.
	 *
	 * @return	float
	 */
	public static function measuringTime() : float {

		return round(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 4);
	}
}