<?php declare(strict_types=1);

namespace GfnFramework\Core\Template;

use GfnFramework\Core\Exceptions\GfnFileException;

/**
 * Class of the Cache-Engine.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Cache {

	private static string $_template_path;
	private static array $_params;
	private static string $_cache_name;

	/**
	 * Sets the needed parameters of the cache.
	 *
	 * @param	string	$template_path	path to the template
	 * @param	array	$params			the parameters
	 *
	 * @return	void
	 */
	public static function setParams(string $template_path, array $params) : void {

		self::$_template_path	= $template_path;
		self::$_params			= $params;

		self::_createCacheName();
	}

	/**
	 * Returns the content of the cache.
	 *
	 * @return	array
	 */
	public static function getCacheContent() : array {

		if (self::_existCache()) {
			return [
				'content'		=> file_get_contents(self::$_cache_name),
				'last_update'	=> filemtime(self::$_cache_name)
			];
		}

		return [];
	}

	/**
	 * Write the content into the cache-file.
	 *
	 * @param	string	$template_rendered
	 *
	 * @return	bool
	 */
	public static function setCacheContent(string $template_rendered) : bool {

		return file_put_contents(self::$_cache_name, $template_rendered) === false;
	}

	/**
	 * Removes all cache-files with expired lifetime.
	 *
	 * @return	void
	 *
	 * @throws	GfnFileException
	 */
	public static function cleanCache() : void {

		$cache_files = glob(PATH_CACHE . '*.cache');

		if ($cache_files === false) {
			throw new GfnFileException('Error while reading cache-files!');
		}

		if (!defined('TEMPLATE_CACHE_LIFETIME')) {
			define('TEMPLATE_CACHE_LIFETIME', TIME_WEEK);
		}

		foreach ($cache_files as $cache_file_path) {
			if (is_file($cache_file_path)) {
				$file_create_time = filectime($cache_file_path);

				if ($file_create_time !== false && (time() - $file_create_time) > TEMPLATE_CACHE_LIFETIME) {
					unlink($cache_file_path);
				}
			}
		}
	}

	/**
	 * Checks if the cache-file exists.
	 *
	 * @return	bool
	 */
	private static function _existCache() : bool {

		if (is_file(self::$_cache_name)) {
			$file_create_time = filectime(self::$_cache_name);

			if ($file_create_time !== false && (time() - $file_create_time) > TEMPLATE_CACHE_LIFETIME) {
				unlink(self::$_cache_name);

				return false;
			}

			return true;
		}

		return false;
	}

	/**
	 * Creates the name of the cache with the parameters.
	 *
	 * @return	void
	 */
	private static function _createCacheName() : void {

		$file_name	= basename(self::$_template_path, '.phtml');
		$param_hash	= hash('xxh3', json_encode(self::$_params));

		self::$_cache_name = PATH_CACHE . $file_name . '_' . $param_hash . '.cache';
	}
}