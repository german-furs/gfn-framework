<?php declare(strict_types=1);

namespace GfnFramework\Core\Template;

use GfnFramework\Core\Exceptions\GfnTemplateException;

/**
 * Class of the Template-Engine.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Engine {

	const string TEMPLATE_MAIN		= PATH_TEMPLATES . 'Page' . DIRECTORY_SEPARATOR . 'main.phtml';
	const string TEMPLATE_EMPTY	= PATH_TEMPLATES . 'empty.phtml';

	/**
	 * Holds the path of the template.
	 *
	 * @var	string
	 */
	private string $_template_path;

	/**
	 * Holds all parameters wich would give to header of the template.
	 *
	 * @var	array
	 */
	private array $_template_header_parameters = [];

	/**
	 * Holds all parameters wich would give to body of the template.
	 *
	 * @var	array
	 */
	private array $_template_body_parameters = [];

	/**
	 * Holds all parameters wich would give to footer of the template.
	 *
	 * @var	array
	 */
	private array $_template_footer_parameters = [];

	/**
	 * Set the path to the template.
	 *
	 * @param	string	$template_path	Path to the template
	 *
	 * @return	void
	 *
	 * @throws	GfnTemplateException
	 */
	public function setTemplatePath(string $template_path) : void {

		if (!is_file($template_path)) {
			throw new GfnTemplateException('The template "' . $template_path . '" does not exist');
		}

		$this->_template_path = $template_path;
	}

	/**
	 * @param	array	$params	Additional parameters for the template
	 *
	 * @return	void
	 */
	public function addHeaderParameters(array $params) : void {

		$this->_template_header_parameters = array_merge($this->_template_header_parameters, $params);
	}

	/**
	 * @param	array	$params	Additional parameters for the template
	 *
	 * @return	void
	 */
	public function addBodyParameters(array $params) : void {

		$this->_template_body_parameters = array_merge($this->_template_body_parameters, $params);
	}

	/**
	 * @param	array	$params	Additional parameters for the template
	 *
	 * @return	void
	 */
	public function addFooterParameters(array $params) : void {

		$this->_template_footer_parameters = array_merge($this->_template_footer_parameters, $params);
	}

	/**
	 * Send the rendered template to the browser.
	 *
	 * @return	void
	 *
	 * @throws	GfnTemplateException
	 */
	public function printPage() : void {

		if (empty($this->_template_path)) {
			$this->_template_path = self::TEMPLATE_EMPTY;
		}

		$cache_params = [
			$this->_template_header_parameters,
			$this->_template_body_parameters,
			$this->_template_footer_parameters
		];

		// Is cache permitted
		if (TEMPLATE_CACHE_ENABLED) {
			Cache::setParams($this->_template_path, $cache_params);
			$cache = Cache::getCacheContent();

			if (empty($cache)) {
				$template_rendered = $this->_render();
				Cache::setCacheContent($template_rendered);
			} else {
				$template_rendered = $cache['content'];
			}
		} else {
			$template_rendered = $this->_render();
		}

		echo $template_rendered;
	}

	/**
	 * Returns the rendered template.
	 *
	 * @return	string
	 *
	 * @throws	GfnTemplateException
	 */
	public function getContent() : string {

		if (empty($this->_template_path)) {
			$this->_template_path = self::TEMPLATE_EMPTY;
		}

		return $this->_render();
	}

	/**
	 * Build up the Template and return the result.
	 *
	 * @return	string
	 *
	 * @throws	GfnTemplateException
	 *
	 * @see		https://gist.github.com/BlackScorp/a22c10325baf554181d30f012f09373f
	 */
	private function _render() : string {

		$header_data	= $this->_buildHeader();
		$footer_data	= $this->_buildFooter();
		$body_template	= $this->_template_path;

		extract($this->_template_body_parameters, EXTR_SKIP);
		ob_start();

		require_once self::TEMPLATE_MAIN;

		return self::_compressTemplate(ob_get_clean());
	}

	/**
	 * Builds the html-header.
	 *
	 * @return	string
	 */
	private function _buildHeader() : string {

		return implode('', $this->_template_header_parameters);
	}

	/**
	 * Builds the html-footer.
	 *
	 * @return	string
	 */
	private function _buildFooter() : string {

		return implode('', $this->_template_footer_parameters);
	}

	/**
	 * Compresses the html-template for output.
	 *
	 * @param	string	$template_rendered	the full rendered html-template
	 *
	 * @return	string
	 */
	private function _compressTemplate(string $template_rendered) : string {

		$search		= [
			'/\>[^\S ]+/s',		// remove whitespaces after tags
			'/[^\S ]+\</s',		// remove whitespaces before tags
			'/(\s)+/s',			// remove multiple whitespace sequences
			'/<!--(.|\s)*?-->/'	// remove comments
		];
		$replace	= ['>','<','\\1'];

		return preg_replace($search, $replace, $template_rendered);
	}
}