<?php declare(strict_types=1);

namespace GfnFramework\Core;

use GfnFramework\Core\Exceptions\GfnFileException;

/**
 * Class to load the Framework, Requests and Properties.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Load {

	/**
	 * @var	array	holds the URI-Request
	 */
	private static array $_uri_request;

	/**
	 * Loads the Framework.
	 *
	 * @return	void
	 *
	 * @throws	GfnFileException
	 */
	public static function init() : void {

		require_once 'constants.php';

		date_default_timezone_set(CORE_TIME_ZONE);
		self::_loadFunctions();
		self::_loadUriRequest();
	}

	/**
	 * Returns the URI-Request.
	 *
	 * @return	array
	 */
	public static function getUriRequest() : array {

		return self::$_uri_request;
	}

	/**
	 * Loads all files under 'Functions'.
	 *
	 * @return	void
	 *
	 * @throws	GfnFileException
	 */
	private static function _loadFunctions() : void {

		$function_files = glob(__DIR__ . DIRECTORY_SEPARATOR . 'Functions' . DIRECTORY_SEPARATOR . '*.php');

		if ($function_files === false) {
			throw new GfnFileException('Error while loading Function-Files');
		}

		foreach ($function_files as $function_file) {
			require_once $function_file;
		}
	}

	/**
	 * Build the request out of the URI.
	 *
	 * @return	void
	 */
	private static function _loadUriRequest() : void {

		$uri_parts		= explode('/', $_SERVER['REQUEST_URI']);
		$uri_part_count	= count($uri_parts);
		$uri_requests	= [];

		if ($uri_part_count > 1) {
			for ($index = 0; $index < $uri_part_count; $index++) {
				if (empty($uri_parts[$index])) {
					continue;
				}

				if ($index === ($uri_part_count - 1)) {
					$query_start	= stripos($uri_parts[$index], '?');
					$uri_request	= $query_start !== false ? substr(strtolower($uri_parts[$index]), 0, $query_start) : strtolower($uri_parts[$index]);

					if (!empty($uri_request)) {
						$uri_requests[]	= $uri_request;
					}

					continue;
				}

				$uri_requests[]	= strtolower($uri_parts[$index]);
			}
		}

		self::$_uri_request = $uri_requests;
	}
}