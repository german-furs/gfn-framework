<?php declare(strict_types=1);

namespace GfnFramework\Core\Exceptions;

use GfnFramework\Core\Email;
use GfnFramework\Core\Template\Engine;
use Exception;
use Throwable;

/**
 * Exception to default framework events.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class GfnException extends Exception {

	/**
	 * Template das E-Mail-Bodys
	 */
	const string MAIL_TPL = 'exception_mail.tpl';

	/**
	 * Log Datei der Exceptions
	 */
	const string LOG_FILE = PATH_LOG . 'error.log';

	/**
	 * @var	array	hält die BCC E-Mail-Adressen
	 */
	protected array $_mail_bcc = [];

	/**
	 * @var	bool	gibt an ob Debug-Informationen in der Mail versendet werden sollen
	 */
	public bool $print_trace = true;

	public function __construct(string $message = '', int $code = 0, ?Throwable $previous = null) {

		parent::__construct($message, $code, $previous);

		$this->_logException();
	}

	/**
	 * Mail mit allen Informationen zur Exception versenden
	 *
	 * @param	string	$to			An zu versendende E-Mail-Adresse
	 * @param	string	$subject	Betreff
	 *
	 * @return	bool
	 *
	 * @throws	GfnTemplateException
	 * @throws	GfnMailException
	 * @throws	Exception
	 */
	public function sendMail(string $to = '', string $subject = '') : bool {

		// Empfänger und Betreff
		$recipient = empty($to) ? EXCEPTION_MAIL_TO : $to;
		$subject = empty($subject) ? get_class($this) : $subject;

		// Body rendern
		$tpl = new Engine();
		$tpl->addBodyParameters(['error_data' => $this]);
		$tpl->setTemplatePath(self::MAIL_TPL);
		$body = $tpl->getContent();

		// Mail verschicken
		$mail = new Email();
		$mail->ClearAllRecipients();
		$mail->addReplyTo($recipient);

		foreach ($this->_mail_bcc as $bcc) {
			if (!empty($bcc)) {
				$mail->addBcc($bcc);
			}
		}

		$mail->setFrom(EXCEPTION_MAIL_FROM);
		$mail->Subject	= $subject;
		$mail->Body		= $body;

		return $mail->send();
	}

	/**
	 * Loggt Exceptions in eine Datei
	 *
	 * @return	void
	 */
	protected function _logException() : void {

		if (!LOG_TO_FILE || CALL_BY_CRON) {
			return;
		}

		$dirname = dirname(self::LOG_FILE);

		if (!file_exists($dirname)) {
			if (!mkdir($dirname, 0755, true)) {
				return;
			}
		}

		$url 		= parse_url($_SERVER['REQUEST_URI']) + ['path' => ''];
		$request	= json_encode($_REQUEST);
		$exception	= explode('\\', get_class($this));
		$log_msg	= sprintf(
			"[%s][%s][%s][%s][%s][%s:%d] %s\n",
			date('d-m-Y H:i:s'),
			$_SERVER['REMOTE_ADDR'],
			$url['path'],
			$request,
			array_pop($exception),
			$this->getFile(),
			$this->getLine(),
			$this->getMessage()
		);

		file_put_contents(self::LOG_FILE, $log_msg, FILE_APPEND);
	}
}