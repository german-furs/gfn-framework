<?php declare(strict_types=1);

namespace GfnFramework\Core\Exceptions;

/**
 * Exception to E-Mail-Events.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class GfnMailException extends GfnException {}