<?php
declare(strict_types=1);

// Define global root path
define('PATH_GFN_MAIN', realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR);

// Define global times in seconds
const TIME_MINUTE	= 60;
const TIME_HOUR		= TIME_MINUTE * 60;
const TIME_DAY		= TIME_HOUR * 24;
const TIME_WEEK		= TIME_DAY * 7;
const TIME_MONTH	= TIME_DAY * 30;
define("TIME_YEAR",	TIME_DAY * (365 + intval(date('L'))));

// Define global settings
define('CALL_BY_CRON',	empty($_SERVER['DOCUMENT_ROOT']));

// Default values for settings by user
if (!defined('PATH_APP')) {
	define('PATH_APP', PATH_GFN_MAIN . 'App' . DIRECTORY_SEPARATOR);
}

if (!defined('PATH_MODULES')) {
	define('PATH_MODULES', PATH_APP . 'Modules' . DIRECTORY_SEPARATOR);
}

if (!defined('PATH_TEMPLATES')) {
	define('PATH_TEMPLATES', PATH_APP . 'Templates' . DIRECTORY_SEPARATOR);
}

if (!defined('PATH_LOG')) {
	define('PATH_LOG', PATH_APP . 'log' . DIRECTORY_SEPARATOR);
}

if (!defined('PATH_TMP')) {
	define('PATH_TMP', PATH_GFN_MAIN . 'App' . DIRECTORY_SEPARATOR);
}

if (!defined('PATH_CACHE')) {
	define('PATH_CACHE', PATH_TMP . 'cache' . DIRECTORY_SEPARATOR);
}

if (!defined('APP_SQLITE_FILE_PATH')) {
	define('APP_SQLITE_FILE_PATH', PATH_TMP . 'sqlite.db');
}

if (!defined('DEV_ENV')) {
	define('DEV_ENV', true);
}

if (!defined('LOG_TO_FILE')) {
	define('LOG_TO_FILE', true);
}

if (!defined('LOG_TO_DB')) {
	define('LOG_TO_DB', false);
}

if (!defined('CORE_TIME_ZONE')) {
	define('CORE_TIME_ZONE', 'UTC');
}

if (!defined('TEMPLATE_CACHE_ENABLED')) {
	define('TEMPLATE_CACHE_ENABLED', true);
}

if (!defined('EXCEPTION_MAIL_TO')) {
	define('EXCEPTION_MAIL_TO', '');
}

if (!defined('EXCEPTION_MAIL_FROM')) {
	define('EXCEPTION_MAIL_FROM', '');
}

if (!defined('SYSTEM_MAIL_FROM')) {
	define('SYSTEM_MAIL_FROM', '');
}

if (!defined('APP_DB_HOST')) {
	define('APP_DB_HOST', '');
}

if (!defined('APP_DB_PORT')) {
	define('APP_DB_PORT', '');
}

if (!defined('APP_DB_NAME')) {
	define('APP_DB_NAME', '');
}

if (!defined('APP_DB_USER')) {
	define('APP_DB_USER', '');
}

if (!defined('APP_DB_PASS')) {
	define('APP_DB_PASS', '');
}