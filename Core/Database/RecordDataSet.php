<?php declare(strict_types=1);

namespace GfnFramework\Core\Database;

use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;
use Exception;

/**
 * Class to define a bundle of fields to a record.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class RecordDataSet {

	/**
	 * Holds the data-fields.
	 *
	 * @var	RecordDataField[]
	 */
	private array $_record_data_fields;

	/**
	 * Holds the primary-fields.
	 *
	 * @var	string[]
	 */
	private array $_primary_fields;

	/**
	 * Creates a RecordDataSet. Needed inputs are:
	 * - 'name' - the name of the column
	 * - 'type' - data-type
	 * - 'properties' - list of data-properties
	 * - ['content'] - if exists the value of the column
	 *
	 * @param	array	$plain_record_data	the needed inputs
	 *
	 * @throws	Exception
	 * @throws	GfnInvalidArgumentException
	 */
	public function __construct(array $plain_record_data) {

		$record_data	= [];

		foreach ($plain_record_data as $row) {
			if (
				!array_key_exists('type', $row)
				|| !array_key_exists('properties', $row)
				|| !array_key_exists('name', $row)
			) {
				throw new GfnInvalidArgumentException('Missing RecordDataField-Elements');
			}

			$current_record_data_field	= new RecordDataField($row['type'], $row['properties']);
			$record_data[$row['name']]	= $current_record_data_field;

			if ($current_record_data_field->isPrimary()) {
				$this->_primary_fields[]	= $row['name'];
			}

			if (array_key_exists('content', $row)) {
				$current_record_data_field->setContent($row['content'], false);
			}
		}

		$this->_record_data_fields	= $record_data;
	}

	/**
	 * Returns the list of all primary-fields.
	 *
	 * @return	RecordDataField[]
	 */
	public function getPrimaries() : array {

		if (empty($this->_primary_fields)) {
			return [];
		}

		$primaries	= [];

		foreach ($this->_primary_fields as $field_name) {
			$primaries[$field_name]	= $this->_record_data_fields[$field_name];
		}

		return $primaries;
	}

	/**
	 * Returns all data-fields.
	 *
	 * @return	RecordDataField[]
	 */
	public function getDataFields() : array {

		return $this->_record_data_fields;
	}

	/**
	 * Returns a selected data-field.
	 *
	 * @param	string	$data_field_name	the name of the data-field
	 *
	 * @return	RecordDataField
	 */
	public function getDataField(string $data_field_name) : mixed {

		return $this->_record_data_fields[$data_field_name];
	}

	/**
	 * Returns the names of the data-fields.
	 *
	 * @return	array
	 */
	public function getDataFieldNames() : array {

		return array_keys($this->_record_data_fields);
	}

	/**
	 * Checks if a primaries are empty.
	 *
	 * @return	bool
	 */
	public function isNew() : bool {

		foreach ($this->getPrimaries() as $record_data_field) {
			if (empty($record_data_field->getContent())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Are there any changes.
	 *
	 * @return	bool
	 */
	public function hasChanges() : bool {

		return !empty($this->changedFields());
	}

	/**
	 * Returns the field-names which has changed else an empty array.
	 *
	 * @return	array
	 */
	public function changedFields() : array {

		$changed_fields	= [];

		foreach ($this->getDataFields() as $data_field_name => $data_field) {
			if ($data_field->isChanged()) {
				$changed_fields[]	= $data_field_name;
			}
		}

		return $changed_fields;
	}
}