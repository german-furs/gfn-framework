<?php declare(strict_types=1);

namespace GfnFramework\Core\Database;

use GfnFramework\Core\Database\Engine\Pdo;
use GfnFramework\Core\Database\Engine\Redis;
use GfnFramework\Core\Exceptions\GfnException;
use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;

/**
 * Class to choose the database-connector.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Connector {

	public const int DATABASE_TYPE_PDO_MYSQL		= 1;
	public const int DATABASE_TYPE_PDO_MSSQL		= 2;
	public const int DATABASE_TYPE_PDO_POSTGRESQL	= 3;
	public const int DATABASE_TYPE_PDO_SQLITE		= 4;
	public const int DATABASE_TYPE_REDIS			= 5;

	private Pdo|Redis $_database;

	/**
	 * @param	int	$database_type	the used type of database
	 *
	 * @throws	GfnException
	 * @throws	GfnInvalidArgumentException
	 */
	public function __construct(int $database_type = self::DATABASE_TYPE_PDO_MYSQL) {

		$this->_database = match ($database_type) {
			self::DATABASE_TYPE_PDO_MYSQL,
			self::DATABASE_TYPE_PDO_MSSQL,
			self::DATABASE_TYPE_PDO_POSTGRESQL,
			self::DATABASE_TYPE_PDO_SQLITE => Pdo::getInstance($database_type),
			self::DATABASE_TYPE_REDIS => Redis::getInstance(),
			default => throw new GfnInvalidArgumentException('Unknown database-type'),
		};
	}

	/**
	 * Returns the database-class.
	 *
	 * @return	Pdo|Redis
	 */
	public function getDatabase() : Pdo|Redis {

		return $this->_database;
	}
}