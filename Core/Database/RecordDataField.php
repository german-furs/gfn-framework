<?php declare(strict_types=1);

namespace GfnFramework\Core\Database;

use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;
use DateTime;
use Exception;

/**
 * Class to define a single field of a record.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class RecordDataField {

	public const int DATA_TYPE_INTEGER		= 1;
	public const int DATA_TYPE_FLOAT		= 2;
	public const int DATA_TYPE_STRING		= 3;
	public const int DATA_TYPE_JSON			= 4;
	public const int DATA_TYPE_DATETIME		= 5;
	public const int DATA_TYPE_TIMESTAMP	= 6;
	public const int DATA_TYPE_DATE			= 7;
	public const int DATA_TYPE_ENUM			= 8;
	public const int DATA_TYPE_BINARY		= 9;

	public const int DATA_PROPERTY_PRIMARY			= 1;
	public const int DATA_PROPERTY_AUTO_INCREMENT	= 2;
	public const int DATA_PROPERTY_UNSIGNED			= 4;
	public const int DATA_PROPERTY_NULL				= 8;
	public const int DATA_PROPERTY_ON_CREATE		= 16;
	public const int DATA_PROPERTY_ON_UPDATE		= 32;
	public const int DATA_PROPERTY_UNIQUE			= 64;

	private int $_type;

	private bool $_is_primary			= false;

	private bool $_is_auto_increment	= false;

	private bool $_is_unsigned			= false;

	private bool $_is_null				= false;

	private bool $_is_on_create			= false;

	private bool $_is_on_update			= false;

	private bool $_is_unique			= false;

	private mixed $_content	= null;

	private bool $_is_changed;

	/**
	 * Creates a RecordDataField object.
	 *
	 * @param	int		$type		the data-type
	 * @param	array	$properties	list of data-properties
	 *
	 * @throws	GfnInvalidArgumentException
	 */
	public function __construct(int $type, array $properties = []) {

		$this->setType($type, false);
		$this->setProperties($properties, false);
	}

	/**
	 * Returns the data-type.
	 *
	 * @return	int
	 */
	public function getType() : int {

		return $this->_type;
	}

	/**
	 * Set the data-type.
	 *
	 * @param	int		$type		the data-type
	 * @param	bool	$is_update	is the current data changed
	 *
	 * @return	void
	 *
	 * @throws	GfnInvalidArgumentException
	 */
	public function setType(int $type, bool $is_update = true) : void {

		if ($type < self::DATA_TYPE_INTEGER || $type > self::DATA_TYPE_ENUM) {
			throw new GfnInvalidArgumentException('Unknown data-type given');
		}

		$this->_type		= $type;
		$this->_is_changed	= $is_update;
	}

	/**
	 * Set the data-properties.
	 *
	 * @param	array	$properties	the list of properties
	 * @param	bool	$is_update	is the current data changed
	 *
	 * @return	void
	 */
	public function setProperties(array $properties, bool $is_update = true) : void {

		$property_sum		= intval(array_sum($properties));
		$this->_is_changed	= $is_update;

		if ($property_sum >= self::DATA_PROPERTY_UNIQUE) {
			$this->_is_unique	= true;
			$property_sum		-= self::DATA_PROPERTY_UNIQUE;
		}

		if ($property_sum >= self::DATA_PROPERTY_ON_UPDATE) {
			$this->_is_on_update	= true;
			$property_sum			-= self::DATA_PROPERTY_ON_UPDATE;
		}

		if ($property_sum >= self::DATA_PROPERTY_ON_CREATE) {
			$this->_is_on_create	= true;
			$property_sum			-= self::DATA_PROPERTY_ON_CREATE;
		}

		if ($property_sum >= self::DATA_PROPERTY_NULL) {
			$this->_is_null	= true;
			$property_sum	-= self::DATA_PROPERTY_NULL;
		}

		if ($property_sum >= self::DATA_PROPERTY_UNSIGNED) {
			$this->_is_unsigned	= true;
			$property_sum		-= self::DATA_PROPERTY_UNSIGNED;
		}

		if ($property_sum >= self::DATA_PROPERTY_AUTO_INCREMENT) {
			$this->_is_auto_increment	= true;
			$property_sum				-= self::DATA_PROPERTY_AUTO_INCREMENT;
		}

		if ($property_sum >= self::DATA_PROPERTY_PRIMARY) {
			$this->_is_primary	= true;
		}
	}

	/**
	 * Sets the content.
	 *
	 * @param	mixed	$content	the content to set
	 * @param	bool	$is_update	is the current data changed
	 *
	 * @return	void
	 *
	 * @throws	Exception
	 */
	public function setContent(mixed $content, bool $is_update = true) : void {

		if (!$this->_checkContent($content)) {
			$content	= $this->_castContent($content);
		}

		$this->_content		= $content;
		$this->_is_changed	= $is_update;
	}

	/**
	 * Returns the content.
	 *
	 * @return	mixed
	 */
	public function getContent() : mixed {

		return $this->_content;
	}

	/**
	 * Is this a primary.
	 *
	 * @return	bool
	 */
	public function isPrimary() : bool {

		return $this->_is_primary;
	}

	/**
	 * Is this an auto increment.
	 *
	 * @return	bool
	 */
	public function isAutoIncrement() : bool {

		return $this->_is_auto_increment;
	}

	/**
	 * Is this an unsigned integer.
	 *
	 * @return	bool
	 */
	public function isUnsigned() : bool {

		return $this->_is_unsigned;
	}

	/**
	 * Is Null allowed.
	 *
	 * @return	bool
	 */
	public function isNull() : bool {

		return $this->_is_null;
	}

	/**
	 * Is set on create.
	 *
	 * @return	bool
	 */
	public function isOnCreate() : bool {

		return $this->_is_on_create;
	}

	/**
	 * Is set on update.
	 *
	 * @return	bool
	 */
	public function isOnUpdate() : bool {

		return $this->_is_on_update;
	}

	/**
	 * Is this a unique value.
	 *
	 * @return	bool
	 */
	public function isUnique() : bool {

		return $this->_is_unique;
	}

	/**
	 * Has the data been changed.
	 *
	 * @return	bool
	 */
	public function isChanged() : bool {

		return $this->_is_changed;
	}

	/**
	 * Checks the content against the datatype.
	 *
	 * @param	mixed	$content	the content to check
	 *
	 * @return	bool
	 */
	private function _checkContent(mixed $content) : bool {

		$result	= false;

		switch ($this->_type) {
			case self::DATA_TYPE_INTEGER:
				$result	= is_int($content);

				break;
			case self::DATA_TYPE_FLOAT:
				$result	=is_float($content);

				break;
			case self::DATA_TYPE_STRING:
				$result	= is_string($content);

				break;
			case self::DATA_TYPE_JSON:
				$result	= (is_string($content) && json_validate($content));

				break;
			case self::DATA_TYPE_DATETIME:
			case self::DATA_TYPE_DATE:
				$result	= is_a($content, 'DateTime');

				break;
			case self::DATA_TYPE_TIMESTAMP:
				$result	= is_numeric($content);

				break;
			case self::DATA_TYPE_ENUM:
				$result	= isset($content);

				break;
			case self::DATA_TYPE_BINARY:
				$result	= is_string($content) && ctype_xdigit($content);
				break;
			default:
				break;
		}

		return $result;
	}

	/**
	 * Casts the content to the datatype.
	 *
	 * @param	mixed	$content	the content to cast
	 *
	 * @return	mixed
	 *
	 * @throws	Exception
	 */
	private function _castContent(mixed $content) : mixed {

		$result	= '';

		switch ($this->_type) {
			case self::DATA_TYPE_INTEGER:
			case self::DATA_TYPE_TIMESTAMP:
				$result	= intval($content);

				break;
			case self::DATA_TYPE_FLOAT:
				$result	=floatval($content);

				break;
			case self::DATA_TYPE_STRING:
			case self::DATA_TYPE_ENUM:
			case self::DATA_TYPE_BINARY:
				$result	= (string) $content;

				break;
			case self::DATA_TYPE_JSON:
				$result	= json_encode((string) $content);

				break;
			case self::DATA_TYPE_DATETIME:
			case self::DATA_TYPE_DATE:
				$result	= new DateTime((string) $content);

				break;
			default:
				break;
		}

		return $result;
	}
}