<?php declare(strict_types=1);

namespace GfnFramework\Core\Database;

use GfnFramework\Core\Exceptions\GfnDbException;
use GfnFramework\Core\Exceptions\GfnException;
use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;
use Exception;

/**
 * Base class of a single record.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
abstract class ActiveRecordAbstract {

	use ActiveRecordTrait;

	/**
	 * Holds the RecordDataSet.
	 *
	 * @see	RecordDataSet::__construct
	 *
	 * @var	RecordDataSet
	 */
	protected RecordDataSet $_record_data_set;

	/**
	 * Creates an instance of ActiveRecord.
	 *
	 * @throws	GfnException
	 * @throws	GfnInvalidArgumentException
	 */
	public function __construct(int $database_type = Connector::DATABASE_TYPE_PDO_MYSQL) {

		if (empty($this->_table_name)) {
			throw new GfnInvalidArgumentException('Table-Name missing');
		}

		if (empty($this->_table_field_set)) {
			throw new GfnInvalidArgumentException('Table-Field-Set missing');
		}

		$this->_record_data_set	= new RecordDataSet($this->_table_field_set);
		$connector				= new Connector($database_type);
		$this->_db				= $connector->getDatabase();
	}

	/**
	 * Removed the content of the called property.
	 *
	 * @param	string	$field_name		the name of the DataField
	 *
	 * @return	void
	 *
	 * @throws	Exception
	 */
	public function __unset(string $field_name) : void {

		if (!isset($this->{$field_name})) {
			return;
		}

		$this->_record_data_set->getDataField($field_name)->setContent(null);
	}

	/**
	 * Returns the content of the called property.
	 *
	 * @param	string	$field_name	the name of the DataField
	 *
	 * @return	mixed
	 */
	public function __get(string $field_name) : mixed {

		if (!isset($this->{$field_name})) {
			return null;
		}

		return $this->_record_data_set->getDataField($field_name)->getContent();
	}

	/**
	 * Sets the content of the called property.
	 *
	 * @param	string	$field_name		the name of the DataField
	 * @param	mixed	$field_value	the content to set
	 *
	 * @return	void
	 *
	 * @throws	Exception
	 */
	public function __set(string $field_name, mixed $field_value) : void {

		if (!isset($this->{$field_name})) {
			return;
		}

		$this->_record_data_set->getDataField($field_name)->setContent($field_value);
	}

	/**
	 * Set the Record manually to new.
	 * Usefully if the primary-field is manually set.
	 *
	 * @return	void
	 */
	public function setFlagNew() : void {

		$this->_is_new_flag	= true;
	}

	/**
	 * Returns the ID of the last insert.
	 * Result is 0 if an error has occurred.
	 *
	 * @return	int
	 */
	public function getLastInsertId() : int {

		return $this->_db->getLastInsertId();
	}

	/**
	 * Loads the Record by database-statement.
	 *
	 * @param	string	$statement	the database-statement
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 * @throws	Exception
	 */
	public function loadFromStatement(string $statement) : void {

		$result_set	= $this->_db->getSingleResultSet($statement);
		$this->_loadResultSet($result_set);
	}

	/**
	 * Loads the Record by WHERE-statement.
	 *
	 * @param	string	$where	the WHERE-statement
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 * @throws	Exception
	 */
	public function loadFromWhere(string $where) : void {

		$statement	= 'SELECT ' . implode(',', $this->_record_data_set->getDataFieldNames());
		$statement	.= ' FROM ' . $this->_table_name;
		$statement	.= ' ' . $where;

		$this->loadFromStatement($statement);
	}

	/**
	 * Loads the current Record.
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 * @throws	Exception
	 */
	public function load() : void {

		$where	= [];

		foreach ($this->_record_data_set->getDataFields() as $data_field_name => $record_data_field) {
			if ($this->{$data_field_name} !== null) {
				$where[]	= $data_field_name . ' = ' . sprintf($this->_getFormat($record_data_field), $this->_db->cast($record_data_field));
			}
		}

		if (empty($where)) {
			return;
		}

		$query		= 'SELECT ' . implode(',', $this->_record_data_set->getDataFieldNames());
		$query		.= ' FROM ' . $this->_table_name;
		$query		.= ' WHERE ' . implode(' AND ', $where);
		$result_set	= $this->_db->getSingleResultSet($query);

		$this->_loadResultSet($result_set);
	}

	/**
	 * Insert or updates the current Record.
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 */
	public function save() : void {

		if (!$this->_record_data_set->hasChanges()) {
			return;
		}

		$where		= ' WHERE';
		$fields		= [];
		$is_insert	= $this->_is_new_flag || $this->_record_data_set->isNew();

		foreach ($this->_record_data_set->changedFields() as $data_field_name) {
			$record_data_field			= $this->_record_data_set->getDataField($data_field_name);
			$fields[$data_field_name]	= sprintf($this->_getFormat($record_data_field), $this->_db->cast($record_data_field));
		}

		if ($is_insert) {
			$query	= 'INSERT INTO ' . $this->_table_name;
			$query	.= ' (' . implode(',', array_keys($fields)) . ') VALUES ';
			$query	.= '(' . implode(',', array_values($fields)) . ');';
		} else {
			$query			= 'UPDATE ' . $this->_table_name . ' SET ';
			$query_parts	= [];

			foreach ($fields as $data_field_name => $data_field_value) {
				$query_parts[]	= $data_field_name . ' = ' . $data_field_value;
			}

			foreach ($this->_record_data_set->getPrimaries() as $data_field_name => $data_field_value) {
				$record_data_field	= $this->_record_data_set->getDataField($data_field_name);
				$where				.= ' ' . $data_field_name . ' = ' . sprintf($this->_getFormat($record_data_field), $data_field_value->getContent());
			}

			$query	.= implode(',', $query_parts);
			$query	.= $where;
		}

		$this->_db->query($query);
		$this->_is_new_flag	= false;
	}

	/**
	 * Deletes the current Record.
	 * Primaries are needed.
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 */
	public function delete() : void {

		$where	= [];

		foreach ($this->_record_data_set->getPrimaries() as $data_field_name => $record_data_field) {
			if ($this->{$data_field_name} !== null) {
				$where[]	= $data_field_name . ' = ' . sprintf($this->_getFormat($record_data_field), $this->_db->cast($record_data_field));
			}
		}

		if (empty($where)) {
			return;
		}

		$query	= 'DELETE FROM ' . $this->_table_name;
		$query	.= ' WHERE ' . implode(' AND ', $where);

		$this->_db->query($query);
		$this->_is_new_flag	= false;
	}

	/**
	 * Loads the Record from the result-set of the database.
	 *
	 * @param	array	$result_set	the result-set of the database
	 *
	 * @return	void
	 *
	 * @throws	Exception
	 */
	private function _loadResultSet(array $result_set) : void {

		$this->_is_new_flag	= false;

		foreach ($this->_record_data_set->getDataFields() as $data_field_name => $record_data_field) {
			if (array_key_exists($data_field_name, $result_set)) {
				$record_data_field->setContent($result_set[$data_field_name], false);
			}
		}
	}
}