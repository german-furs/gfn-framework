<?php declare(strict_types=1);

namespace GfnFramework\Core\Database;

use GfnFramework\Core\Database\Engine\Pdo;
use GfnFramework\Core\Database\Engine\Redis;
use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;
use DateTime;

/**
 * Trait of the ActiveRecord-Classes.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
trait ActiveRecordTrait {

	/**
	 * Holds the name of the table.
	 *
	 * @var	string
	 */
	protected string $_table_name;

	/**
	 * Holds the definition of the columns.
	 *
	 * @var	array
	 */
	protected array $_table_field_set;

	/**
	 * Holds the database-class.
	 *
	 * @var	Pdo|Redis
	 */
	protected Pdo|Redis $_db;

	/**
	 * Holds the manually set flag to identify the Record as new.
	 *
	 * @var	bool
	 */
	private bool $_is_new_flag = false;

	/**
	 * Checks if the property exists.
	 *
	 * @param	string	$field_name		the name of the DataField
	 *
	 * @return	bool
	 *
	 * @throws	GfnInvalidArgumentException
	 */
	public function __isset(string $field_name) : bool {

		if (!in_array($field_name, array_column($this->_table_field_set, 'name'))) {
			throw new GfnInvalidArgumentException('Table-Field unknown');
		}

		return true;
	}

	/**
	 * Returns the name of the table.
	 *
	 * @return	string
	 */
	public function getTableName() : string {

		return $this->_table_name;
	}

	/**
	 * Returns the default field-set of the table.
	 *
	 * @return	array
	 */
	public function getTableFieldSet() : array {

		return $this->_table_field_set;
	}

	/**
	 * Returns a format for the use with sprintf of the RecordDataField.
	 * - %d - int / float
	 * - '%s' - all others
	 *
	 * @see		sprintf
	 *
	 * @param	RecordDataField	$record_data_field	instance of RecordDataField
	 *
	 * @return	string
	 */
	protected function _getFormat(RecordDataField $record_data_field) : string {

		return (
		$record_data_field->getType() === RecordDataField::DATA_TYPE_INTEGER
		|| $record_data_field->getType() === RecordDataField::DATA_TYPE_FLOAT
		|| $record_data_field->getType() === RecordDataField::DATA_TYPE_TIMESTAMP
			? '%d' : "'%s'"
		);
	}

	/**
	 * Returns a formatted IN-WHERE-Clause.
	 *
	 * @link	https://www.w3schools.com/mysql/mysql_in.asp
	 *
	 * @param	RecordDataField	$record_data_field	instance of RecordDataField
	 * @param	array			$values				the values to set inside de IN()
	 *
	 * @return	string
	 */
	protected function _getInList(RecordDataField $record_data_field, array $values) : string {

		if (
			$record_data_field->getType() === RecordDataField::DATA_TYPE_INTEGER
			|| $record_data_field->getType() === RecordDataField::DATA_TYPE_FLOAT
			|| $record_data_field->getType() === RecordDataField::DATA_TYPE_TIMESTAMP
		) {
			$in_list	= ' IN (' . implode(',', array_unique($values)) . ')';
		} else {
			if (
				$record_data_field->getType() === RecordDataField::DATA_TYPE_DATETIME
				|| $record_data_field->getType() === RecordDataField::DATA_TYPE_DATE
			) {
				$format	= ($record_data_field->getType() === RecordDataField::DATA_TYPE_DATETIME ? 'Y-m-d H:i:s' : 'Y-m-d');
				$values	= $this->castDateTimeToString($values, $format);
			}

			$in_list	= " IN ('" . implode("','", array_unique($values)). "')";
		}

		return $in_list;
	}

	/**
	 * Casts a DateTime object to formatted string.
	 *
	 * @param	array	$values	the array with DateTime objects
	 * @param	string	$format	the format to cast
	 *
	 * @return	array
	 */
	protected function castDateTimeToString(array $values, string $format) : array {

		$values_casted	= [];

		/**
		 * @var	DateTime	$value
		 */
		foreach ($values as &$value) {
			$values_casted[]	= $value->format($format);
		}

		return $values_casted;
	}
}