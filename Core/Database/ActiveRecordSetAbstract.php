<?php declare(strict_types=1);

namespace GfnFramework\Core\Database;

use ArrayIterator;
use GfnFramework\Core\Exceptions\GfnDbException;
use GfnFramework\Core\Exceptions\GfnException;
use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;
use Exception;

/**
 * Base class of a set of records.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
abstract class ActiveRecordSetAbstract extends ArrayIterator {

	use ActiveRecordTrait;

	/**
	 * Holds the pointer of the ArrayIterator.
	 *
	 * @var	int
	 */
	protected int $_pointer	= 0;

	/**
	 * Holds a list of RecordDataSet.
	 *
	 * @var	RecordDataSet[]
	 */
	protected array $_record_data_sets = [];

	/**
	 * Creates an instance of ActiveRecordSet.
	 *
	 * @throws	GfnException
	 * @throws	GfnInvalidArgumentException
	 */
	public function __construct(int $database_type = Connector::DATABASE_TYPE_PDO_MYSQL) {

		if (empty($this->_table_name)) {
			throw new GfnInvalidArgumentException('Table-Name missing');
		}

		if (empty($this->_table_field_set)) {
			throw new GfnInvalidArgumentException('Table-Field-Set missing');
		}

		$connector	= new Connector($database_type);
		$this->_db	= $connector->getDatabase();

		parent::__construct($this->_record_data_sets, ArrayIterator::ARRAY_AS_PROPS);
	}

	/**
	 * Removed the content of the called property of the current pointer.
	 *
	 * @param	string	$field_name		the name of the DataField
	 *
	 * @return	void
	 *
	 * @throws	Exception
	 */
	public function __unset(string $field_name) : void {

		if (!isset($this->{$field_name}) || !array_key_exists($this->key(), $this->_record_data_sets)) {
			return;
		}

		$this->_record_data_sets[$this->key()]->getDataField($field_name)->setContent(null);
	}

	/**
	 * Returns the content of the called property of the current pointer.
	 *
	 * @param	string	$field_name	the name of the DataField
	 *
	 * @return	mixed
	 */
	public function __get(string $field_name) : mixed {

		if (!isset($this->{$field_name}) || !array_key_exists($this->key(), $this->_record_data_sets)) {
			return null;
		}

		return $this->_record_data_sets[$this->key()]->getDataField($field_name)->getContent();
	}

	/**
	 * Sets the content of the called property of the current pointer.
	 *
	 * @param	string	$field_name		the name of the DataField
	 * @param	mixed	$field_value	the content to set
	 *
	 * @return	void
	 *
	 * @throws	Exception
	 */
	public function __set(string $field_name, mixed $field_value) : void {

		if (!isset($this->{$field_name}) || !array_key_exists($this->key(), $this->_record_data_sets)) {
			return;
		}

		$this->_record_data_sets[$this->key()]->getDataField($field_name)->setContent($field_value);
	}

	/**
	 * Adds a new RecordDataSet.
	 *
	 * @param	RecordDataSet	$record_data_set	the RecordDataSet to add
	 *
	 * @return	void
	 */
	public function add(RecordDataSet $record_data_set) : void {

		$this->_record_data_sets[]	= $record_data_set;
	}

	/**
	 * Removes a single RecordDataSet.
	 *
	 * @param	int	$record_key	the key of the RecordDataSet
	 *
	 * @return	void
	 */
	public function remove(int $record_key) : void {

		unset($this->_record_data_sets[$record_key]);
	}

	/**
	 * Removes all RecordDataSet.
	 *
	 * @return	void
	 */
	public function clear() : void {

		$this->_record_data_sets	= [];
		$this->_is_new_flag			= false;
		$this->rewind();
	}

	/**
	 * Resets the pointer.
	 *
	 * @return	void
	 */
	public function rewind() : void {

		$this->_pointer	= 0;
	}

	/**
	 * Checks if the RecordDataSet of the current pointer is valid.
	 *
	 * @return	bool
	 */
	public function valid() : bool {

		return isset($this->_record_data_sets[$this->_pointer]);
	}

	/**
	 * Steps the pointer forward.
	 *
	 * @return	void
	 */
	public function next() : void {

		$this->_pointer++;
	}

	/**
	 * Returns the current pointer.
	 *
	 * @return	int
	 */
	public function key() : int {

		return $this->_pointer;
	}

	/**
	 * Returns the RecordDataSet of the current pointer.
	 *
	 * @return	RecordDataSet
	 */
	public function current() : RecordDataSet {

		return $this->_record_data_sets[$this->_pointer];
	}

	/**
	 * Counts the RecordDataSet.
	 *
	 * @return	int
	 */
	public function count() : int {

		return count($this->_record_data_sets);
	}

	/**
	 * Loads the Records by database-statement.
	 *
	 * @param	string	$statement	the database-statement
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 * @throws	GfnInvalidArgumentException
	 */
	public function loadFromStatement(string $statement) : void {

		$result_set	= $this->_db->getResultSet($statement);
		$this->_loadResultSet($result_set);
	}

	/**
	 * Loads the Records by WHERE-statement.
	 *
	 * @param	string	$where	the WHERE-statement
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 * @throws	GfnInvalidArgumentException
	 */
	public function loadFromWhere(string $where) : void {

		$record_data_set	= new RecordDataSet($this->_table_field_set);
		$statement			= 'SELECT ' . implode(',', $record_data_set->getDataFieldNames());
		$statement			.= ' FROM ' . $this->_table_name;
		$statement			.= ' ' . $where;

		$this->loadFromStatement($statement);
	}

	/**
	 * Loads the records.
	 *
	 * @return	void
	 *
	 * @throws	GfnInvalidArgumentException
	 * @throws	GfnDbException
	 * @throws	Exception
	 */
	public function load() : void {

		$search_data	= [];

		foreach ($this as $record_data_set) {
			if ($record_data_set->hasChanges()) {
				foreach ($record_data_set->changedFields() as $data_field_name) {
					$search_data[$data_field_name][]	= $record_data_set->getDataField($data_field_name)->getContent();
				}
			}
		}

		if (empty($search_data)) {
			return;
		}

		$where				= [];
		$record_data_set	= new RecordDataSet($this->_table_field_set);

		foreach ($search_data as $data_field_name => $values) {
			$where[]	= $data_field_name . $this->_getInList($record_data_set->getDataField($data_field_name), $values);
		}

		$query		= 'SELECT ' . implode(',', $record_data_set->getDataFieldNames());
		$query		.= ' FROM ' . $this->_table_name;
		$query		.= ' WHERE ' . implode(' AND ', $where);
		$result_set	= $this->_db->getResultSet($query);

		$this->_loadResultSet($result_set);
	}

	/**
	 * Inserts or updates the Records.
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 */
	public function save() : void {

		$query_insert	= '';
		$query_inserts	= [];
		$query_updates	= [];

		foreach ($this as $record_data_set) {
			if ($record_data_set->isNew() || $this->_is_new_flag) {
				if (empty($query_insert)) {
					$query_insert	= 'INSERT INTO ' . $this->_table_name;
					$query_insert	.= ' (' . implode(',', array_values($record_data_set->getDataFieldNames())) . ') VALUES ';
				}

				$field_values	= [];

				foreach ($record_data_set->getDataFields() as $record_data_field) {
					$field_values[]	= sprintf($this->_getFormat($record_data_field), $this->_db->cast($record_data_field));
				}

				$query_inserts[]	= '(' . implode(',', $field_values) . ')';
			} elseif ($record_data_set->hasChanges()) {
				$changed_record_data_fields	= [];

				foreach ($record_data_set->changedFields() as $field_name) {
					$record_data_field				= $record_data_set->getDataField($field_name);
					$changed_record_data_fields[]	= $field_name . ' = ' . sprintf($this->_getFormat($record_data_field), $this->_db->cast($record_data_field));
				}

				if (empty($changed_record_data_fields)) {
					continue;
				}

				$where	= [];

				foreach ($record_data_set->getPrimaries() as $field_name => $record_data_field) {
					$where[]	= $field_name . ' = ' . sprintf($this->_getFormat($record_data_field), $this->_db->cast($record_data_field));
				}

				if (empty($where)) {
					continue;
				}

				$query_update		= 'UPDATE ' . $this->_table_name . ' SET ';
				$query_update		.= implode(',', $changed_record_data_fields);
				$query_update		.= ' WHERE ' . implode(' AND ', $where);
				$query_updates[]	= $query_update;
			}
		}

		if (!empty($query_inserts)) {
			$query				= $query_insert . implode(',', $query_inserts) . ';';
			$this->_db->query($query);
			$this->_is_new_flag	= false;
		}

		if (!empty($query_updates)) {
			$query	= implode(';', $query_updates);
			$this->_db->query($query);
		}
	}

	/**
	 * Deletes all Records.
	 * Primaries are needed.
	 *
	 * @return	void
	 *
	 * @throws	GfnInvalidArgumentException
	 * @throws	GfnDbException
	 * @throws	Exception
	 */
	public function delete() : void {

		$record_data_set_ids	= [];
		$where					= [];

		foreach ($this as $record_data_set) {
			$primaries	= $record_data_set->getPrimaries();

			foreach ($primaries as $data_field_name => $record_data_field) {
				$record_data_set_ids[$data_field_name][]	= $record_data_field->getContent();
			}
		}

		if (empty($record_data_set_ids)) {
			return;
		}

		$record_data_set	= new RecordDataSet($this->_table_field_set);

		foreach ($record_data_set_ids as $data_field_name => $rows) {
			$record_data_field	= $record_data_set->getDataField($data_field_name);
			$where[]			= $data_field_name . $this->_getInList($record_data_field, $rows);
		}

		$query	= 'DELETE FROM ' . $this->_table_name;
		$query	.= ' WHERE ' . implode(' AND', $where);

		$this->_db->query($query);
		$this->clear();
	}

	/**
	 * Loads the Records from the result-set of the database.
	 *
	 * @param	array	$result_set	the result-set of the database
	 *
	 * @return	void
	 *
	 * @throws	GfnInvalidArgumentException
	 */
	private function _loadResultSet(array $result_set) : void {

		$this->clear();

		foreach ($result_set as $record_data_set_raw) {
			$table_field_set	= $this->_table_field_set;

			foreach ($table_field_set as &$table_field) {
				if (!array_key_exists($table_field['name'], $record_data_set_raw)) {
					continue;
				}

				$table_field['content']	= $record_data_set_raw[$table_field['name']];
			}

			$this->add(new RecordDataSet($table_field_set));
		}
	}
}