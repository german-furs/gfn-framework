<?php declare(strict_types=1);

namespace GfnFramework\Core\Database\Engine;

use GfnFramework\Core\Abstracts\Singleton;
use GfnFramework\Core\Database\Connector;
use GfnFramework\Core\Database\RecordDataField;
use GfnFramework\Core\Exceptions\GfnDbException;
use GfnFramework\Core\Exceptions\GfnInvalidArgumentException;
use DateTime;
use PDOException;
use PDOStatement;

/**
 * PDO-Database-Class.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Pdo extends Singleton {

	/**
	 * Holds the PDO-Connector.
	 *
	 * @var	\PDO
	 */
	private \PDO $_db_connector;

	/**
	 * Holds the database-type.
	 *
	 * @var	int
	 */
	private int $_database_type;

	/**
	 * Holds the last PDOStatement.
	 *
	 * @var	PDOStatement
	 */
	private PDOStatement $_statement;

	/**
	 * Initializes the database-connector.
	 *
	 * @param	int	$database_type	the used type of database
	 *
	 * @throws	GfnInvalidArgumentException
	 * @throws	GfnDbException
	 */
	public function __construct(int $database_type) {

		$this->_database_type	= $database_type;

		switch ($database_type) {
			case Connector::DATABASE_TYPE_PDO_MYSQL:
			case Connector::DATABASE_TYPE_PDO_MSSQL:
			case Connector::DATABASE_TYPE_PDO_POSTGRESQL:
			case Connector::DATABASE_TYPE_PDO_SQLITE:
				$this->_connect();

				break;
			default:
				throw new GfnInvalidArgumentException();
		}
	}

	/**
	 * Executes a SQL statement.
	 *
	 * @param	string	$query	the SQL statement
	 *
	 * @return	bool
	 *
	 * @throws	GfnDbException
	 */
	public function query(string $query) : bool {

		if (empty($query)) {
			return false;
		}

		try {
			$statement	= $this->_db_connector->prepare($query);

			if ($statement !== false) {
				$statement->execute();
				$this->_statement	= $statement;

				return true;
			}
		} catch (PDOException $e) {
			throw new GfnDbException($e->getMessage(), $e->getCode(), $e);
		}

		return false;
	}

	/**
	 * Executes a SQL statement and returns the results.
	 *
	 * @param	string	$query	the SQL statement
	 *
	 * @return	array
	 *
	 * @throws	GfnDbException
	 */
	public function getResultSet(string $query) : array {

		if (!$this->query($query)) {
			return [];
		}

		$result_set	= $this->_statement->fetchAll(\PDO::FETCH_ASSOC);

		if (empty($result_set)) {
			return [];
		}

		return $result_set;
	}

	/**
	 * Executes a SQL statement and returns a single result.
	 *
	 * @param	string	$query	the SQL statement
	 *
	 * @return	array
	 *
	 * @throws	GfnDbException
	 */
	public function getSingleResultSet(string $query) : array {

		$result_set	= $this->getResultSet($query);

		if (empty($result_set)) {
			return [];
		}

		return $result_set[0];
	}

	/**
	 * Casts a RecordDataField to a PDO safe data-type.
	 *
	 * @param	RecordDataField	$record_data_field	the RecordDataField to cast
	 *
	 * @return	mixed
	 */
	public function cast(RecordDataField $record_data_field) : mixed {

		switch ($record_data_field->getType()) {
			case RecordDataField::DATA_TYPE_DATETIME:
				$format		= 'Y-m-d H:i:s';
				/**
				 * @var	DateTime	$datetime
				 */
				$datetime	= $record_data_field->getContent();

				return $datetime->format($format);

			case RecordDataField::DATA_TYPE_DATE:
				$format		= 'Y-m-d';
				/**
				 * @var	DateTime	$datetime
				 */
				$datetime	= $record_data_field->getContent();

				return $datetime->format($format);
			default:
				return $record_data_field->getContent();
		}
	}

	/**
	 * Returns the last insert ID if exists.
	 *
	 * @return	int
	 */
	public function getLastInsertId() : int {

		$last_insert_id	= $this->_db_connector->lastInsertId();

		if ($last_insert_id === false) {
			return 0;
		}

		return intval($last_insert_id);
	}

	/**
	 * Initializes a connection to a database.
	 *
	 * @return	void
	 *
	 * @throws	GfnDbException
	 */
	private function _connect() : void {

		if (!empty($this->_db_connector)) {
			return;
		}

		$dsn	= '';

		switch ($this->_database_type) {
			case Connector::DATABASE_TYPE_PDO_MYSQL:
				$dsn	.= 'mysql:host=' . APP_DB_HOST . ';port=' . APP_DB_PORT . ';dbname=' . APP_DB_NAME;

				break;
			case Connector::DATABASE_TYPE_PDO_MSSQL:
				$dsn	.= 'sqlsrv:Server=' . APP_DB_HOST . ',' . APP_DB_PORT . ';Database=' . APP_DB_NAME;

				break;
			case Connector::DATABASE_TYPE_PDO_POSTGRESQL:
				$dsn	.= 'pgsql:host=' . APP_DB_HOST . ';port=' . APP_DB_PORT . ';dbname=' . APP_DB_NAME;
				$dsn	.= ';user=' . APP_DB_USER . ';password=' . APP_DB_PASS;

				break;
			case Connector::DATABASE_TYPE_PDO_SQLITE:
				$dsn	.= 'sqlite:' . APP_SQLITE_FILE_PATH;

				break;
		}

		try {
			$this->_db_connector	= new \PDO($dsn, APP_DB_USER, APP_DB_PASS);
			$this->_db_connector->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		} catch (PDOException $e) {
			throw new GfnDbException($e->getMessage(), $e->getCode(), $e);
		}
	}
}