<?php declare(strict_types=1);

namespace GfnFramework\Core\Database\Engine;

use GfnFramework\Core\Abstracts\Singleton;

/**
 * Redis-Database-Class.
 *
 * @copyright	2024 Ivo Arndt
 * @license		LGPL-3.0-or-later
 * @package		Core
 * @since		0.0.1
 */
class Redis extends Singleton {

	public function __construct() {}
}