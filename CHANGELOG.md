# GFN-Framework Changelog

## Alpha

### 0.0.5
*(2024-11-21)*
- Moved the check of the constant TEMPLATE_CACHE_LIFETIME to Cache.php
- Translated an Error in Cache.php
### 0.0.4
*(2024-11-21)*
- Moved the constants into a separated file
- Removed GfnFramework.php
### 0.0.3
*(2024-11-21)*
- Fix in Composer-Path
### 0.0.2
*(2024-11-21)*
- Rebuild Framework to Library
- Added missing datatypes to the constants in GfnException.php
### 0.0.1
*(2024-03-18)*
- Project initialization